#!/usr/bin/python3

"""Validate a given .gitlab-ci.yaml file against the GitLab API
server. This is useful if you maintain a collection of template files
that are *not* named `.gitlab-ci.yaml` and will therefore *not* be
checked by the GitLab CI on push. This should be added to a
`.gitlab-ci.yml` file to be effective. It expects a
GITLAB_PRIVATE_TOKEN from the environment, which should be a user
token with API privileges that should be injected in the project's
private variables."""


import argparse
import logging
import os
import sys

import requests


class LoggingAction(argparse.Action):
    """change log level on the fly"""

    def __init__(self, *args, **kwargs):
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)
        # cargo-culted from _StoreConstAction
        setattr(ns, self.dest, self.const or values)


def main():
    logging.basicConfig(format="%(levelname)s: %(message)s", level="INFO")
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-q",
        "--quiet",
        action=LoggingAction,
        const="WARNING",
        help="silence messages except warnings and errors",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    parser.add_argument(
        "--api-url",
        default=os.environ.get("CI_API_V4_URL", "https://gitlab.com/api/v4"),
        help="""GitLab API URL endpoint,
        defaults to $CI_API_V4_URL environment or %(default)s if undefined""",
    )
    parser.add_argument(
        "path",
        nargs="+",
        type=argparse.FileType("r", encoding="utf-8"),
        help="GitLab CI YAML files to validate",
    )
    args = parser.parse_args()

    token = os.environ.get("GITLAB_PRIVATE_TOKEN")
    if token is None:
        logging.error("missing GITLAB_PRIVATE_TOKEN environment, aborting")
        sys.exit(1)
    headers = {
        "Content-Type": "application/json",
        "PRIVATE-TOKEN": token,
    }
    success = True
    for path in args.path:
        logging.info("linting GitLab CI YAML file: %s", path.name)
        payload = {
            "content": path.read(),
        }
        logging.debug("sending paylod: %r", payload)
        r = requests.post(
            args.api_url + "/ci/lint",
            headers=headers,
            json=payload,
        )
        # XXX: a bit rough, we should handle errors here better
        r.raise_for_status()
        j = r.json()
        logging.debug("response: %r", j)
        if j["valid"]:
            logging.info("valid file: %s", path.name)
        else:
            logging.error("invalid file: %s", path.name)
            success = False
            for w in j["warnings"]:
                logging.warning(w)
            for e in j["errors"]:
                logging.error(e)
    if not success:
        sys.exit(2)


if __name__ == "__main__":
    main()
